import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { LibraryService } from '../library.service';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})
export class LibraryComponent implements OnInit {

  offset = 0;
  private limit = 10;
  library = [];
  

  constructor(private librarySvc: LibraryService, private router: Router,
      private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    console.log('in ngOnInit')
    this.offset = this.librarySvc.cursor;
    this.loadLibrary();
  }

  private loadLibrary() {
    this.librarySvc.getAllLibrary({offset: this.offset, limit: this.limit })
      .then((result) => { this.library = result })
      .catch((error) => { console.error(error); });
  }

  prev() {
    this.offset = this.offset - this.limit;
    this.librarySvc.cursor = this.offset;
    this.loadLibrary(); //becareful
  }

  next() {
    this.offset = this.offset + this.limit;
    this.librarySvc.cursor = this.offset;
    this.loadLibrary(); //becareful
  }

  showDetails(id: number) {
    console.log('> id: %d', id);
    this.router.navigate([ '/library', id ]);
  }

}
