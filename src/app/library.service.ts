import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LibraryService {

  readonly SERVER = 'http://localhost:3000'

  cursor = 0;

  constructor(private httpClient: HttpClient) { }

  // GET /library/<id>
  getLibrary(id: number): Promise<any> {
    return (
      this.httpClient.get(`${this.SERVER}/library/${id}`)
        .take(1).toPromise()
    );
  }

  // GET /library?limit=<n>&offset=<n>
  getAllLibrary(config = {}): Promise<any> {
    //Set the query string
    let qs = new HttpParams();
    qs = qs.set('limit', config['limit'] || 10)
            .set('offset', config['offset'] || 0);
    return (
      this.httpClient.get(`${this.SERVER}/library`, { params: qs })
        .take(1).toPromise()
    );
  }

}
