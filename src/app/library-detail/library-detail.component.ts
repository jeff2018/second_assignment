import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LibraryService } from '../library.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-library-detail',
  templateUrl: './library-detail.component.html',
  styleUrls: ['./library-detail.component.css']
})
export class LibraryDetailComponent implements OnInit {

  library = {};
  id = 0;

  private id$: Subscription;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private librarySvc: LibraryService) { }

  ngOnInit() {
    console.log("LibraryDetail ngOnInit: " , this.id$);
    this.id$ = this.activatedRoute.params.subscribe(
      (param) => {
        console.log('> param  = ', param);
        this.id = parseInt(param.id);
        this.librarySvc.getLibrary(this.id)
          .then((result) => this.library = result)
          .catch((error) => {
            alert(`Error: ${JSON.stringify(error)}`);
          });
      }
    )
  }

  ngOnDestroy() {
    this.id$.unsubscribe();
  }

  prev() {
    this.id = this.id - 1;
    this.router.navigate(['/library', this.id])
  }

  next() {
    this.id = this.id + 1;
    this.router.navigate(['/library', this.id])
  }

  goBack() {
    this.router.navigate(['/' ]);
  }

}
