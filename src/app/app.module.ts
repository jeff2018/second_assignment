import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LibraryComponent } from './library/library.component';
import { LibraryDetailComponent } from './library-detail/library-detail.component';
import { LibraryService } from './library.service';
import { LibrarySearchComponent } from './library-search/library-search.component';

const routes: Routes = [
  { path: '', component: LibraryComponent },
  { path: 'library', component: LibraryComponent },
  { path: 'library/:id', component: LibraryDetailComponent },
  { path: '**', redirectTo: '/', pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    LibraryComponent,
    LibraryDetailComponent,
    LibrarySearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [LibraryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
