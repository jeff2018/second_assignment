const CONFIG = {
    host: 'localhost', port: 3306,
    user: 'jeff', password: 'jeff',
    database: 'library',
    connectionLimit: 4
};

module.exports = CONFIG;