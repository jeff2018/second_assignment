//load libraries
const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const cors = require('cors');

//load config
const mysqlConfig = require('./mysql_config');
const pool = mysql.createPool(mysqlConfig);

const mkQuery = (SQL, pool) => {
    return (params) => {
        const p = new Promise((resolve, reject) => {
            console.log("In closure: ", SQL);
            pool.getConnection((err, conn) => {
                if (err) {
                    reject({status: 500, error: err}); return;
                }
                conn.query(SQL, params || [],
                    (err, result) => {
                        try {
                            if (err)
                                reject(err);
                            else
                                reject({status: 400, error: err}); return;
                        } finally {
                            conn.release();
                        }
                    }
                )
            })
        })
        return (p);
    }
}

const app = express();

//set cors
app.use(cors());

//routes
const SELECT_LIBRARY = 'select id, title, author_firstname ,author_lastname from books limit ? offset ?';
const selectlibrary = mkQuery(SELECT_LIBRARY, pool);
app.get('/library', (req, resp) => {
    const limit = req.query.limit|| 10; 
    const offset = req.query.offset|| 0; 

    pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(SELECT_LIBRARY, [ parseInt(limit), parseInt(offset) ],
           (error, results) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); return;
                   }
                   resp.status(200).json(results);
               } finally {
                   conn.release();
               }
           }
       )
   });
});

//search function in development
/*const SEARCH_LIBRARY = "select title, author_lastname, author_firstname from books WHERE (title LIKE '%?%' or author_lastname LIKE '%?%' or author_firstname LIKE '%?%' )";
const searchlibrary = mkQuery(SEARCH_LIBRARY, pool);
app.get('/library', (req, resp) => {
    const limit = req.query.limit|| 10; 
    const offset = req.query.offset|| 0; 

    pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(SEARCH_LIBRARY, [ parseInt(limit), parseInt(offset), req.params.title, req.params.author_lastname, req.params.author_firstname ],
           (error, results) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); return;
                   }
                   resp.status(200).json(results);
               } finally {
                   conn.release();
               }
           }
       )
   });
});*/

//display each book details
const SELECT_BY_LIBRARY_ID = 'select * from books where id = ?';
const selectByLibraryId = mkQuery(SELECT_BY_LIBRARY_ID, pool);
app.get('/library/:id', (req, resp) => {
    console.log(`library id = ${req.params.id}`);

   pool.getConnection((error, conn) => {
       if (error) {
           resp.status(500).json({error: error}); return;
       }
       conn.query(SELECT_BY_LIBRARY_ID, [req.params.id],
           (error, result) => {
               try {
                   if (error) {
                       resp.status(400).json({error: error}); 
                       return;
                   }
                   if (result.length)
                       resp.status(200).json(result[0]);
                   else
                       resp.status(404).json({error: 'Not Found'});
               } finally { conn.release() ;}
           }
       )
   })
});

//start the application
const PORT = process.argv[2] || process.env.APP_PORT || 3000;
app.listen(PORT, () => {
    console.log('Application started on port %d', PORT);
});